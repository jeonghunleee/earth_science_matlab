%% Wrote with MATLAB R2018b 
%% Team '물리가 좋아' 야간
%   201501655 이정훈 (조장)
%   201601787 이승수
%   201601791 정영빈  
%   201600315 문창준
%   201600329 이승규
%% declaring the variables
temp=[];
temp_low=[];
temp_high=[];

mean_temp=[];
mean_temp_low=[];
mean_temp_high=[];

days_of_spring=[];
days_of_summer=[];
days_of_autumn=[];
days_of_winter=[];

rain=[];
sum_rain=[];

%% data parsing

for i=1908:2018
    % passing odd data sets
    % 기준 : 365일의 데이터가 모두 존재해야 함
    if i>=1950 && i<=1953
        continue
    end
    
    % read a csv file
    % 일평균기온만 가져옴
    i_temp=csvread('earth_data/'+string(i)+'.csv',1,2,[1,2,365,2])';
    i_temp_low=csvread('earth_data/'+string(i)+'.csv',1,3,[1,3,365,3])';
    i_temp_high=csvread('earth_data/'+string(i)+'.csv',1,5,[1,5,365,5])';
    i_rain = csvread('earth_data/'+string(i)+'.csv',1,12,[1,12,365,12])';
    % 계절 일수 계산
    % 봄: 평균 5~20 최저 0 이상
    % 여름: 평균 20 이상
    % 가을: 평균 5~20
    % 겨울: 평균 5이하 최저 0 이하
    i_temp_spring=0;
    i_temp_summer=0;
    i_temp_autumn=0;
    i_temp_winter=0;
    for i=1:365
        if (i_temp(i) >= 20) % summer
            i_temp_summer = i_temp_summer + 1;
        end
        if ((i_temp_low(i) <= 0) && (i_temp(i) <= 5)) %winter
            i_temp_winter = i_temp_winter + 1;
        end
        if ((i_temp(i) >= 5) && (i_temp(i) <= 20) && (i_temp_low(i) >= 0) && (i < (365/2)))
            i_temp_spring = i_temp_spring + 1;
        end
        if ((i_temp(i) >= 5) && (i_temp(i) <= 20) && (i_temp_low(i) >= 0) && (i >= (365/2)))
            i_temp_autumn = i_temp_autumn + 1;
        end
    end
    days_of_spring=[days_of_spring i_temp_spring];
    days_of_summer=[days_of_summer i_temp_summer];
    days_of_autumn=[days_of_autumn i_temp_autumn];
    days_of_winter=[days_of_winter i_temp_winter];
    
    % appending datas
    rain = [rain i_rain];
    sum_rain= [sum_rain sum(i_rain)];
    
    temp=[temp i_temp];
    temp_low=[temp_low i_temp_low];
    temp_high=[temp_high i_temp_high];
    
    mean_temp=[mean_temp mean(i_temp)];
    mean_temp_low=[mean_temp_low mean(i_temp_low)];
    mean_temp_high=[mean_temp_high mean(i_temp_high)];
    
    
end

%% visualizations
years=1:length(mean_temp);
day= 1:365;
total_day= 1:length(temp);
figure(1);clf;
subplot(211);plot(total_day,temp);title(['\fontsize{14pt}물리가 좋아 조 ' newline '\fontsize{10pt}Temperature ( mean of the days )']);
subplot(212);
p_t=polyfit(years,mean_temp,4);
plot(years,mean_temp,1:.1:years(end),polyval(p_t,1:.1:years(end)),'r');title('\mu of Temperature ( mean of the days )');
xlabel('years');
ylabel('temperature (°C)');
figure(2);clf;
subplot(211);plot(temp_low);title(['\fontsize{14pt}물리가 좋아 조 ' newline '\fontsize{10pt}Temperature ( the lowest of the days )']);
subplot(212);
p_tl=polyfit(years,mean_temp_low,4);
plot(years,mean_temp_low,1:.1:years(end),polyval(p_tl,1:.1:years(end)),'r');title('\mu of Temperature ( the lowest of the days )');
xlabel('years');
ylabel('temperature (°C)');
figure(3);clf;
subplot(211);plot(temp_high);title(['\fontsize{14pt}물리가 좋아 조 ' newline '\fontsize{10pt}Temperature ( the highest of the days )']);
subplot(212);
p_th=polyfit(years,mean_temp_high,4);
plot(years,mean_temp_high,1:.1:years(end),polyval(p_th,1:.1:years(end)),'r');title('\mu of Temperature ( the highest of the days )');
xlabel('years');
ylabel('temperature (°C)');
% 여기서 시나리오 : (일기준) 최저온도, 최고온도, 평균온도 모두 같은 양상으로
%                  그려지므로, 앞으론 일 평균온도 데이터를 기반으로 자료를 만들어가면 됨


% 봄여름가을겨울별 날짜수 및
% 최소자승법 3차 곡선(polyfitted)
figure(4);clf;
% spring
subplot(411);
p_spr=polyfit(years,days_of_spring,3);
plot(years,days_of_spring,1:.1:107,polyval(p_spr,1:.1:107),'r');
title(['\fontsize{14pt}물리가 좋아 조 ' newline '\fontsize{10pt}days of spring']);
ylabel('temperature (°C)');
%summer
subplot(412);
p_smr=polyfit(years,days_of_summer,3);
plot(years,days_of_summer,1:.1:107,polyval(p_smr,1:.1:107),'r');
title('days of summer');
ylabel('temperature (°C)');
%autumn
subplot(413);
p_aut=polyfit(years,days_of_autumn,3);
plot(years,days_of_autumn,1:.1:107,polyval(p_aut,1:.1:107),'r');
title('days of autumn');
ylabel('temperature (°C)');
%winter
subplot(414);
p_wtr=polyfit(years,days_of_winter,3);
plot(years,days_of_winter,1:.1:107,polyval(p_wtr,1:.1:107),'r');
title('days of winter');
xlabel('years','fontsize',20);
ylabel('temperature (°C)');

% 여기서 시나리오 : 우린 항상 봄,가을이 짧아진 것 같다고 말한다.해당 그래프가의 최근 20~30년 값이 이를 증명
%                  + 겨울의 날짜수는 하락 추세 (즉, 평균이하로 감소) + 여름의 날짜가 증가
%                  + 이는 곧 아열대 기후로 이미 거의 변했음을 증명

figure(5);clf;
subplot(211);plot(total_day,rain);
title(['\fontsize{14pt}물리가 좋아 조 ' newline '\fontsize{10pt}amount of precipitation']);
ylabel('daily precipitation (mm)');
subplot(212);
p_r = polyfit(years,sum_rain,5);
p_r2=polyfit(years,sum_rain,1);
plot(years,sum_rain,1:.1:years(end),polyval(p_r,1:.1:years(end)),'r',1:.1:years(end),polyval(p_r2,1:.1:years(end)),'k');
title('\Sigma of precipitaiton');
xlabel('years');
ylabel('\Sigma precipitation (mm)');
% 여기서 시나리오 : 검은선(1차 최소자승직선)은 평균적으로 강수량의 증가를 보여줌
%                  + 빨간선(5차 최소자승곡선)은 최근 약 10년간의 강수량이 급하강 함을 보임
%                  + 조사된 자료와 같은 양상임을 증명
